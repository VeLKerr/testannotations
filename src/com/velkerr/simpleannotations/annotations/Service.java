package com.velkerr.simpleannotations.annotations;

import java.lang.annotation.*;

/**
 * Created by velkerr on 29.09.16.
 */
@Documented
@Inherited
@Target(value = ElementType.TYPE)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Service {
    String name();

    boolean lazyLoad() default false;
}
