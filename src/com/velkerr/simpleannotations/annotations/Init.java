package com.velkerr.simpleannotations.annotations;

import java.lang.annotation.*;

/**
 * Created by velkerr on 29.09.16.
 */

@Target(value = ElementType.METHOD)
@Retention(value = RetentionPolicy.RUNTIME)
@Inherited
public @interface Init {
    boolean suppressException() default false;
}
