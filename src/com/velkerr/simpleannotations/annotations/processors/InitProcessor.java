package com.velkerr.simpleannotations.annotations.processors;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import java.util.Set;

/**
 * Created by velkerr on 29.09.16.
 */
@SupportedAnnotationTypes({"com.velkerr.simpleannotations.annotations.Init"})
@SupportedSourceVersion(SourceVersion.RELEASE_7)
public class InitProcessor extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        for (TypeElement te: annotations){
            System.out.println(te.getSimpleName());
        }
        return true;
    }
}
