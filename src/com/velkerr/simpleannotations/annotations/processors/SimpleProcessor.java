package com.velkerr.simpleannotations.annotations.processors;

import com.velkerr.simpleannotations.annotations.Init;
import com.velkerr.simpleannotations.annotations.Service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Created by velkerr on 29.09.16.
 */
public class SimpleProcessor {
    private static final Map<String, Object> context = new HashMap<>();
    private static final List<Object> unInitServices = new ArrayList<>();

    public static Object getServiceByName(String name){
        Object instance = context.get(name);
        if(instance != null && unInitServices.contains(instance)){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    invokeInits(instance.getClass(), instance);
                }
            }).start();
            unInitServices.remove(instance);
        }
        return instance;
    }

    public static void initService(String className) {
        try {
            Class<?> service = Class.forName(className);
            if (service.isAnnotationPresent(Service.class)) {
                Object serviceInstance = service.newInstance();
                context.put(service.getAnnotation(Service.class).name(), serviceInstance);
                if(!service.getAnnotation(Service.class).lazyLoad()){
                    invokeInits(service, serviceInstance);
                }
                else{
                    unInitServices.add(serviceInstance);
                }
            }
        }
        catch (ClassNotFoundException | IllegalAccessException | InstantiationException ex){
            System.out.println("Some error happened!");
        }
    }

    private static void invokeInits(Class<?> service, Object serviceInstance){
        for (Method method: service.getMethods()){
            if(method.isAnnotationPresent(Init.class)){
                try {
                    method.invoke(serviceInstance);
                    System.out.println("Method was invoked correctly");
                } catch (InvocationTargetException | IllegalAccessException e) {
                    if(method.getAnnotation(Init.class).suppressException()){
                        throw new RuntimeException(e.getClass().getSimpleName() + " happened");
                    }
                    else{
                        System.err.println("Exception happened!");
                    }
                }
            }
        }
    }
}
