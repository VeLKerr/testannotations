package com.velkerr.simpleannotations;

import com.velkerr.simpleannotations.annotations.processors.SimpleProcessor;

/**
 * Created by velkerr on 29.09.16.
 */
public class Main {
    public static void main(String[] args) {
        SimpleProcessor.initService("com.velkerr.simpleannotations.services.SimpleService");
        SimpleProcessor.initService("com.velkerr.simpleannotations.services.LazyService");
        SimpleProcessor.initService("java.lang.String");

        System.out.println(SimpleProcessor.getServiceByName("Service2"));
    }
}
