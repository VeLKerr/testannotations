package com.velkerr.simpleannotations.services;

import com.velkerr.simpleannotations.annotations.Service;

/**
 * Suppose, that this service requires much time to initialize.
 * It would be fine if we provide lazy initialization.
 * I.e., we initialize the service when it will be used and not before
 *
 * Created by velkerr on 29.09.16.
 */
@Service(name = "Service2", lazyLoad = true)
public class LazyService extends ParentService{
    private static final int WAITING_TIME = 500;

    @Override
    public void init() {
        try {
            Thread.sleep(WAITING_TIME);
        }
        catch (InterruptedException ex){
            //Nothing to do
        }
        super.init();
    }
}
