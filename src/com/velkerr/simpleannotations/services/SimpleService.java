package com.velkerr.simpleannotations.services;

import com.velkerr.simpleannotations.annotations.Service;

/**
 * Service with aggressive initialization.
 *
 * Created by velkerr on 29.09.16.
 */
@Service(name = "Service1")
public class SimpleService extends ParentService{
}
