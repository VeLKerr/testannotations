package com.velkerr.simpleannotations.services;

import com.velkerr.simpleannotations.annotations.Init;

/**
 * Created by velkerr on 28.09.16.
 */
public abstract class ParentService {

    /**
     * Service initializer
     */
    @Init
    public void init(){
        System.out.println(this.getClass().getSimpleName() + ": Init invoked");
    }

    /**
     * Method to test <code>AnnotationProcessins</code> with not annotated methods.
     */
    public void test(){
        System.out.println("Testing");
    }
}
